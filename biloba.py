#!/bin/env python3
#
# Copyright (c) 2022 Michael McCune
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
import argparse
from http import HTTPStatus
import http.server
import io
import logging
import json
import sys

html_template = '''
<html>
<head>
<title>Biloba</title>
<style>
span.highlight {{
    background: #bababa;
    display: block;
}}
</style>
</head>
<body>
{body}
</body>
</html>
'''

index = html_template.format(body='Not generated')

class Suite:
    def __init__(self, suite):
        self.reports = []
        self.description = suite.get('SuiteDescription')
        if self.description is None or len(self.description) == 0:
            self.description = 'No suite description set'

        self.path = suite.get('SuitePath')
        if self.path is None or len(self.path) == 0:
            self.path = 'No suite path set'

        for i, report in enumerate(suite.get('SpecReports', [])):
            if report.get('State') != 'passed':
                continue

            try:
                newreport = Report(report)
                logging.info(f'processed report for {newreport.filename}@{newreport.linenumber}')
                self.append_report(newreport)
            except Exception as ex:
                nl = report.get('LeafNodeLocation', {})
                fn = nl.get('FileName')
                ln = nl.get('LineNumber')
                logging.error(f'error processing report for {fn}@{ln}')

    def append_report(self, report):
        self.reports.append(report)
        self.reports = sorted(self.reports, key=lambda r: r.hierarchy)


class Report:
    def __init__(self, report):
        hierarchy = report.get('ContainerHierarchyTexts')
        if hierarchy is None or len(hierarchy) == 0:
            hierarchy = ['No hierarchy defined']
        hierarchy = ' / '.join(hierarchy)
        if len(hierarchy) == 0:
            hierarchy = 'No hierarchy text set'
        self.hierarchy = hierarchy
        logging.debug(self.hierarchy)

        self.text = report.get('LeafNodeText')
        if self.text is None:
            self.text = 'No leaf node text set'

        leafnodeloc = report.get('LeafNodeLocation', {})
        self.filename = leafnodeloc.get('FileName')
        self.linenumber = leafnodeloc.get('LineNumber')

        self.nodetype = report.get('LeafNodeType', '')




class BilobaHttpRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        logging.info(self.path)
        if self.path == '/':
            content = index
            body = content.encode('UTF-8', 'replace')
            self.send_response(HTTPStatus.OK)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', str(len(body)))
            self.end_headers()
            self.wfile.write(body)
        elif 'favicon' in self.path:
            super().do_GET()
        else:
            # if not the index, then try to load the file and inject in an html wrapper
            try:
                path, param = self.path.split('?', maxsplit=1)
                linenumber = int(param.split('=', maxsplit=1)[1])
                logging.debug(f'attempting to load {path}, highlighting linenumber {linenumber}')
                content = '<pre>\n'
                with open(path) as fp:
                    lines = fp.read().splitlines()
                    for i, line in enumerate(lines):
                        if i+1 == linenumber:
                            content += f'<span id="highlighted-test" class="highlight">{line}</span>'
                        else:
                            content += line
                        content += '\n'
                content += '</pre>'
                content = html_template.format(body=content)
                body = content.encode('UTF-8', 'replace')
                self.send_response(HTTPStatus.OK)
                self.send_header('Content-Type', 'text/html')
                self.send_header('Content-Length', str(len(body)))
                self.end_headers()
                self.wfile.write(body)
            except Exception as ex:
                logging.debug(ex)
                super().do_GET()


def main(filename):
    fp = open(filename)
    report = json.load(fp)

    suites = []
    for i, suite in enumerate(report):
        try:
            newsuite = Suite(suite)
            logging.info(f'created suite for {newsuite.path}')
            suites.append(newsuite)
        except Exception:
            logging.error(f'error processing suite at index {i}')
            continue

    body = ''
    suites = sorted(suites, key=lambda s: s.description)
    for suite in suites:
        if len(suite.reports) == 0:
            continue

        body += f'<h1>{suite.description}</h1>\n'

        body += '<ul>'
        for report in suite.reports:
            body += f'<li>{report.hierarchy}<ul>'
            if len(report.nodetype) > 0 :
                body += f'<li>{report.nodetype} {report.text}</li>'
            else:
                body += f'<li>{report.text}</li>'
            body += f'<li><a href="{report.filename}?linenumber={report.linenumber}#highlighted-test" target="_blank">{report.filename}@{report.linenumber}</a></li></ul></li>'
        body += '</ul>'

    global index
    index = html_template.format(body=body)


    server_address = ('127.0.0.1', 8080)
    httpd = http.server.HTTPServer(server_address, BilobaHttpRequestHandler)
    try:
        print('serving at http://127.0.0.1:8080/')
        httpd.serve_forever()
    except KeyboardInterrupt:
        logging.warning('\nKeyboard interrupt received, exiting...')
        sys.exit(0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Reformat info from a Ginkgo test report JSON file")
    parser.add_argument('filename', help='the json file to process')
    parser.add_argument('--debug', action='store_true', help='turn on debug logging')
    args = parser.parse_args()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    main(args.filename)
